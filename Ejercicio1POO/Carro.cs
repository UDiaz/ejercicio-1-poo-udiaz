﻿using System;

namespace Ejercicio1POO
{
// Clase
class Carro
    {
     
    // Atributos
    public string marca;
    public string modelo;
    public string color;
    public string placa;
    public int año;
     
    //Métodos
    public Carro(string marca,string modelo,string color,string placa, int año)
    {
        //marca = "Renault";
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
        this.placa = placa;
        this.año = año;
    }

    }
}