﻿using System;

namespace Ejercicio1POO
{
    class Program
    {
        static void Main(string[] args)
        {
            Carro micarro = new Carro("Dodge","Stratus ST","Blanco","JER7250",2006);
           // micarro.marca = "Dodge";
            Console.WriteLine("La marca de mi carro es:"+" "+ micarro.marca);
            Console.WriteLine("El modelo de mi carro es:"+" "+ micarro.modelo);
            Console.WriteLine("El color de mi carro es:"+" "+ micarro.color);
            Console.WriteLine("Las placas de mi carro son:"+" "+ micarro.placa);
            Console.WriteLine("El año de mi carro es:"+" "+ micarro.año);
        }
    }
}
